var Pack = require('../package');

module.exports = {
    server: {
        useUrlSettings: true, // Use when debugging
        host: 'localhost',
        port: 3000,
        enableOpsLogging: false,
        opsLoggingFrequency: 1000
    },
    database: {
        host: 'localhost',
        port: 27017,
        db: 'cachingservicepocdb',
        username: '',
        password: ''
    },
    swagger: {
        info: {
            'title': 'API Documentation',
            'version': Pack.version
        }
    },
    appSettings: {
        cacheMaxAge: 10 // Minutes
    },
    apiDetails: {
        getPatron: {
            host: 'http://RD15',
            port: '8000',
            url: '/api/v1/patrons/'
        }
    }
};