'use strict';

var Hapi = require('hapi'),
    Good = require('good'),
    Config = require('./configs/config'),
    Db = require('./configs/database'),
    HapiSwagger = require('hapi-swagger'),
    Inert = require('inert'),
    Vision = require('vision'),
    host = process.env.virtualDirPath,
    port = process.env.PORT,
    opsLogging;

// Define server
if (Config.server.useUrlSettings) {
    host = Config.server.host;
    port = Config.server.port;
}

var server = new Hapi.Server();
server.connection({
    host: host,
    port: port
});

// Define routes
var DiagnosticsRoute = require('./routes/diagnosticsRoute');
var PatronRoute = require('./routes/patronRoute');

// Initialise routes
server.route(DiagnosticsRoute.endpoints);
server.route(PatronRoute.endpoints);

// Logging properties
if (Config.server.enableOpsLogging) {
    opsLogging = {
        interval: Config.server.opsLoggingFrequency
    };
}
else {
    opsLogging = false;
}

var goodOptions = {
    ops: opsLogging,
    reporters: {
        console: [{
            module: 'good-console',
            args: [{
                log: '*',
                request: '*',
                response: '*',
                error: '*',
                ops: '*'
            }]
        }, 'stdout']
    }
};

// Start server
server.register([
    Inert,
    Vision,
    {
        register: Good,
        options: goodOptions
    },
    {
        register: HapiSwagger,
        options: Config.swagger
    }
], (err) => {
    if (err) {
        throw err;
    }

    server.start((err) => {
        if (err) {
            throw err;
        }
        server.log('info', 'Server running at: ' + server.info.uri);
    });
});