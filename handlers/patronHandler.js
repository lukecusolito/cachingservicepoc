var Boom = require('boom'),
    Patron = require('../models/patronModel');

exports.GetPatron = function (request, reply) {
    Patron.findOne({
        'PatronId': request.params.id
    }, function (err, patron) {
        if (err || !patron) {
            reply(Boom.badRequest("Problem getting patron", err));
            return;
        }
        reply(patron);
    });
}