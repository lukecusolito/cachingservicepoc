var Mongoose = require('mongoose'),
    Pack = require('../package');

exports.IsAlive = function (request, reply) {
    var DbConnectionState = (Mongoose.connection.readyState === 1);
    reply({
        "ServerAlive": true,
        "DbConnectionState": DbConnectionState
    });
};
exports.GetVersion = function (request, reply) {
    reply({
        "ServerVersion": Pack.version,
        "Dependencies": Pack.dependencies
    });
};