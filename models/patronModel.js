var Mongoose = require('mongoose'),
    Schema = Mongoose.Schema;

var PatronSchema = new Schema({
    PatronId: { type: String, required: true, unique: true }
});

var Patron = Mongoose.model('Patron', PatronSchema);

module.exports = Patron;