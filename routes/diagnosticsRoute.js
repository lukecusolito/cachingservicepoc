var DiagnosticsHandler = require('../handlers/diagnosticsHandler');

exports.endpoints = [
    {
        method: 'GET',
        path: '/api/diagnostics/isalive',
        handler: DiagnosticsHandler.IsAlive,
        config: {
            tags: ['api', 'Diagnostics', 'GET'],
            description: 'Gets server status',
            notes: 'Returns server status if server is alive'
        }
    },
    {
        method: 'GET',
        path: '/api/diagnostics/version',
        handler: DiagnosticsHandler.GetVersion,
        config: {
            tags: ['api', 'Diagnostics', 'GET'],
            description: 'Gets server package versions',
            notes: 'Returns server version and associated package versions'
        }
    }
];