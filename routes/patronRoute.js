var PatronHandler = require('../handlers/patronHandler');

exports.endpoints = [
    {
        method: 'GET',
        path: '/api/v1/patrons/{id}',
        handler: PatronHandler.GetPatron,
        config: {
            tags: ['api', 'Retrieves', 'GET'],
            description: 'Gets patron details',
            notes: 'Returns all information for patron'
        }
    }
];